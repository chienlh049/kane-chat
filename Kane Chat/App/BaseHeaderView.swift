//
//  BaseHeaderView.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 19/04/2023.
//

import Foundation
import VNavigator
import SwiftUI

struct BaseHeaderView<Content: View>: AppNavigator {
    var content: Content
    var header: String
    
    init(
        header: String = "Header",
        @ViewBuilder content:() -> Content) {
        self.header = header
        self.content = content()
    }
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "chevron.backward")
                    .onTapGesture {
                        navigator.pop()
                    }
                Text(header)
                Spacer()
            }
            .foregroundColor(.white)
            .padding(.horizontal, 16)
            Spacer()
            content
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(.black)
    }
}

struct BaseHeaderView_Preview: PreviewProvider {
    static var previews: some View {
        BaseHeaderView {
            
        }
    }
}
