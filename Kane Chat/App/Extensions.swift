//
//  Extensions.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 20/04/2023.
//

import Foundation
import SwiftUI


protocol ImageModifier {
    /// `Body` is derived from `View`
    associatedtype Body : View
    
    /// Modify an image by applying any modifications into `some View`
    func body(image: Image) -> Self.Body
}

extension Image {
    func modifier<M>(_ modifier: M) -> some View where M: ImageModifier {
        modifier.body(image: self)
    }
}

struct MyImageModifier: ImageModifier {
    let width: CGFloat = 128
    let height: CGFloat = 128
    
    func body(image: Image) -> some View {
        image
            .resizable()
            .scaledToFill()
            .frame(width: width, height: height)
            .clipShape(Circle())
            .overlay {
                Circle()
                    .stroke(.gray.opacity(0.5), lineWidth: 1)
            }
    }
}

struct MyButtonStype: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(16)
            .frame(maxWidth: .infinity)
            .background(.orange)
            .cornerRadius(16)
    }
}
