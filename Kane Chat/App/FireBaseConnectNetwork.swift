//
//  FireBaseConnectNetwork.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 19/04/2023.
//

import Foundation
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift

class ConnectFireBase {
    let db = Firestore.firestore()
    static let shared = ConnectFireBase()
    private init() {}
    
    func getData<T: Codable>
    (collection: String,
     parsingType: T.Type,
     successBlock: @escaping (T) -> Void,
     failBlock: @escaping (String) -> Void) {
        
        self.getIdFireBase(collection: collection) { id in
            self.db.collection(collection).document(id).getDocument(as: T.self) { result in
                switch result {
                case .success(let success):
                    successBlock(success)
                case .failure(let failure):
                    failBlock(failure.localizedDescription)
                }
            }
        }
        
    }
    
    private func getIdFireBase(collection: String, completion: @escaping (String) -> Void) {
        db.collection(collection).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    completion(document.documentID)
                }
            }
        }
    }
}
