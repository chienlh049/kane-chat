//
//  ShowAlert.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 20/04/2023.
//

import SwiftUI
import VNavigator

struct ShowAlert: AppNavigator {
    var message: String = ""
    
    var body: some View {
        VStack {
            Text("Kane Chat")
                .bold()
            Rectangle()
                .foregroundColor(.gray.opacity(0.5))
                .font(.title)
                .frame(width: .infinity, height: 1)
            Text(message)
                .padding()
            
            Button("OK") {
                navigator.dismiss()
            }
            .padding()
            .frame(maxWidth: .infinity)
            .background(Color.blue)
            .foregroundColor(.white)
            .cornerRadius(10)
        }
        .padding()
        .background(Color.white)
        .cornerRadius(20)
        .shadow(radius: 10)
        .padding()
    }
}

struct ShowAlert_Previews: PreviewProvider {
    static var previews: some View {
        ShowAlert()
    }
}
