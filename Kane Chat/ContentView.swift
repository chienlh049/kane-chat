//
//  ContentView.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 18/04/2023.
//

import SwiftUI
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift
import VNavigator

struct ContentView: AppNavigator {
    @StateObject var viewModel = ContentViewModel()
    @State var isLoginMode: Bool = false
    var body: some View {
        NavigationView {
            VStack(spacing: 16) {
                
                header()
                
                Spacer()
                
                contentBody()
                
                buttonContent()
                
                Spacer()
            }
            .padding()
            .padding(.vertical, 16)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(.black).ignoresSafeArea()
            .fullScreenCover(isPresented: $viewModel.shouldShowImagePicker) {
                ImagePicker(image: $viewModel.image)
            }
        }
    }
    
    func header() -> some View {
        VStack {
            Picker(selection: $isLoginMode, label: Text("Picker")) {
                Text("log in")
                    .tag(true)
                Text("Sign up")
                    .tag(false)
            }
            .cornerRadius(8)
            .pickerStyle(SegmentedPickerStyle())
            .background(.gray.opacity(0.5))
            .padding(.top)
        }
    }
    
    func contentBody() -> some View {
        VStack(spacing: 16) {
            if !isLoginMode {
                Button {
                    //
                    viewModel.shouldShowImagePicker.toggle()
                } label: {
                    if let image = viewModel.image {
                        Image(uiImage: image)
                            .modifier(MyImageModifier())
                    } else {
                        Image(systemName: "person.fill")
                            .modifier(MyImageModifier())
                    }
                }
            }
            
            TextField("User Name", text: $viewModel.userName)
                .textFieldStyle(TextFieldStype())
            SecureField("Pass word", text: $viewModel.password)
                .textFieldStyle(TextFieldStype())
        }
    }
    
    func buttonContent() -> some View {
        VStack {
            if isLoginMode {
                Button {
                    viewModel.logIn {
                        navigator.pushToView(view: HomeScreen())
                    }
                    
                } label: {
                    Text("Log in")
                }
                .buttonStyle(MyButtonStype())
            } else {
                Button {
                    viewModel.signUp {
                        navigator.presentView(view: ShowAlert(message: "Sign up success"), presentStyle: .overFullScreen)
                    }
                } label: {
                    Text("Sign up")
                }
                .buttonStyle(MyButtonStype())
            }
        }
    }
}


struct TextFieldStype: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(16)
            .background(.white)
            .cornerRadius(8)
    }
}

struct ContentModel: Codable {
    var user: String
    var password: String
}
//
//struct SourceImage: Codable {
//    var avatar: String?
//    var backgroundImage: String?
//}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

