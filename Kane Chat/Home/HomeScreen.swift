//
//  HomeScreen.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 19/04/2023.
//

import SwiftUI
import VNavigator

struct HomeScreen: AppNavigator {
    var body: some View {
        VStack(spacing: 8) {
            header()
            
            ItemChat(
                avatar: "person.crop.circle",
                name: "chien",
                status: "online",
                lastMesage: "message") {
                    // onTap Message
                }
            
            Spacer()
            
        }
        .navigationBarHidden(true)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(.black)
    }
    
    func header() -> some View {
        HStack {
            Image(systemName: "lineweight")
                .onTapGesture {
                    navigator.pushToView(view: MenuScreen())
                }
            Spacer()
            Text("")
                .font(.system(size: 24))
                .bold()
            Spacer()
            Image(systemName: "person.crop.circle.fill")
                .resizable()
                .frame(width: 30, height: 30)
                .clipShape(Circle())
                .overlay {
                    Circle().stroke(.gray.opacity(0.5), lineWidth: 1)
                }
                .onTapGesture {
                    
                }
        }.padding(.horizontal, 16)
        .foregroundColor(.white)
        .padding(.bottom, 8)
    }
}

struct ItemChat: View {
    var avatar: String
    var name: String
    var status: String
    var lastMesage: String
    var onTapMessage: () -> Void
    var body: some View {
        HStack(spacing: 8) {
            Image(systemName: avatar)
                .resizable()
                .frame(width: 40, height: 40)
                .clipShape(Circle())
                .shadow(radius: 2)
                .foregroundColor(.white)
            
            VStack(alignment: .leading, spacing: 0) {
                HStack(spacing: 8) {
                    Text(name)
                        .font(.system(size: 18))
                    Text(status)
                        .font(.system(size: 12))
                        .opacity(0.5)
                }
                Text(lastMesage)
                    .font(.system(size: 16))
                    .opacity(0.7)
            }
            .foregroundColor(.white)
            
            Spacer()
        }
        .padding(16)
        .onTapGesture {
            onTapMessage()
        }
        
    }
}

struct HomeScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreen()
    }
}


