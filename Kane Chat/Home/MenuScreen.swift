//
//  MenuScreen.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 19/04/2023.
//

import SwiftUI
import VNavigator

struct MenuScreen: AppNavigator {
    var body: some View {
        BaseHeaderView(header: "Menu") {
            
        }
    }
}

struct MenuScreen_Previews: PreviewProvider {
    static var previews: some View {
        MenuScreen()
    }
}
