//
//  Kane_ChatApp.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 18/04/2023.
//

import SwiftUI

@main
struct Kane_ChatApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
