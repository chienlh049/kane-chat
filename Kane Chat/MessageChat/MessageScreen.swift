//
//  MainMessageScreen.swift
//  Kane Chat
//
//  Created by Chiến Lê on 25/04/2023.
//

import SwiftUI

struct MainMessageScreen: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct MainMessageScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainMessageScreen()
    }
}
