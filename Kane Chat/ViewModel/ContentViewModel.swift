//
//  ContentViewModel.swift
//  Kane Chat
//
//  Created by ECO0691-CHIENLH on 19/04/2023.
//

import Foundation
import FirebaseAuth
import Firebase
import FirebaseStorage

class ContentViewModel: ObservableObject {
    @Published var userInfo: ContentModel?
    @Published var userName: String = ""
    @Published var password: String = ""
    @Published var image: UIImage?
    @Published var shouldShowImagePicker: Bool = false
    
    func getData() {
        ConnectFireBase.shared.getData(collection: "userInfo", parsingType: ContentModel.self) { response in
            self.userInfo = response
        } failBlock: { errorMsg in
            
        }
    }
    
    func logIn(completion: @escaping () -> Void) {
        FirebaseManager.shared.auth.signIn(withEmail: self.userName, password: self.password) { authenResult, err in
            if let err = err {
                print("log in fail with: \(err.localizedDescription)")
                self.navigator.presentView(view: ShowAlert(message: err.localizedDescription), presentStyle: .overFullScreen)
            }
            else {
                print("log in success with user: \(self.userName)")
                completion()
            }
        }
    }
    
    func signUp(completion: @escaping () -> Void) {
        FirebaseManager.shared.auth.createUser(withEmail: self.userName, password: self.password) { authenResult, err in
            if let err = err {
                print("create user fail with: \(err.localizedDescription)")
                self.navigator.presentView(view: ShowAlert(message: err.localizedDescription), presentStyle: .overFullScreen)
                return
            }
            print("create user success with user: \(self.userName)")
            self.userName = ""
            self.password = ""
            self.persistImageToStorage()
            completion()
        }
    }
    
    private func persistImageToStorage() {
        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
        let ref = FirebaseManager.shared.storage.reference(withPath: uid)
        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
        ref.putData(imageData, metadata: nil) { metadata, err in
            if let _ = err {
                return
            }
            ref.downloadURL { url, err in
                if let _ = err {
                    return
                }
                guard let url = url else {
                    return
                }
                self.storeUserInformation(imageProfileUrl: url)
                print(url.absoluteString)
            }
        }
    }
    
    private func storeUserInformation(imageProfileUrl: URL) {
        guard let uid = FirebaseManager.shared.auth.currentUser?.uid else { return }
        let userData = ["email": self.userName, "uid": uid, "profileImageUrl": imageProfileUrl.absoluteString]
        FirebaseManager.shared.firestore.collection("userInfo")
            .document(uid).setData(userData) { err in
                if let err = err {
                    print(err)
                    return
                }
                
                print("Success")
            }
    }
}

class FirebaseManager: NSObject {
    
    let auth: Auth
    let storage: Storage
    let firestore: Firestore
    
    static let shared = FirebaseManager()
    
    override init() {
        
        self.auth = Auth.auth()
        self.storage = Storage.storage()
        self.firestore = Firestore.firestore()
        
        super.init()
    }
    
}
